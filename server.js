const express = require('express');
const path = require('path');
const { checkRFC4566, checkRFC4570, checkST2110 } = require('sdpoker');
const app = express();

const port = parseInt(process.env.PORT, 10) || 5000;

const TestEnum = Object.freeze({
  checkRFC4566: 1,
  checkRFC4570: 2,
  checkST2110: 3,
});

const TestFlags = [
  'checkEndings',
  'whitespace',
  'should',
  'noCopy',
  'noMedia',
  'duplicate',
  'videoOnly',
  'audioOnly',
  'channelOrder',
  'shaping',
  'useIP4',
  'useIP6',
  'multicast',
  'unicast',
];

// SDPoker Functions
function checkSDP(test, params, sdp) {
  let returnVar = { test: '', params, sdp: sdp, errors: [] };
  let rfc4566Errors = [];
  let rfc4570Errors = [];
  let st2110Errors = [];
  let errors = [];
  if (test >= TestEnum.checkRFC4566) {
    rfc4566Errors = checkRFC4566(sdp, params);
  }
  if (test >= TestEnum.checkRFC4570) {
    rfc4570Errors = checkRFC4570(sdp, params);
  }
  if (test >= TestEnum.checkST2110) {
    st2110Errors = checkST2110(sdp, params);
  }
  errors = rfc4566Errors.concat(rfc4570Errors, st2110Errors);
  console.log(`checkSDP(): Found ${errors.length} error(s) in SDP file:`);
  for (let c in errors) {
    console.log(`${+c + 1}: ${errors[c].message}`);
  }
  returnVar.test = Object.keys(TestEnum).find((key) => TestEnum[key] === test);
  errors.forEach((err) => {
    let obj = {};
    obj.message = err.message;
    if (err.message.startsWith('Line ')) {
      let arr = err.message.split(':');
      obj.line = parseInt(arr[0].substring(5), 10);
    }
    returnVar.errors.push(obj);
  });
  return returnVar;
}

// Check-Request
app.post('/api/check', express.text({ type: '*/*' }), (req, res) => {
  let test = TestEnum.checkST2110;
  let params = {};
  let sdp = req.body;
  if (Object.keys(sdp).length === 0) {
    res.status(400).send('Body is Empty!');
    return;
  }
  if (TestEnum.hasOwnProperty(req.query.test)) test = TestEnum[req.query.test];
  TestFlags.forEach((flag) => {
    if (req.query.hasOwnProperty(flag)) {
      params[flag] = true;
    }
  });
  let retVal = checkSDP(test, params, sdp);
  res.writeHead(200);
  res.end(`${JSON.stringify(retVal)}`);
});

//Disallow all other GET Requests to the API
app.get('/api*', (req, res) => {
  res.status(400).send('GET is not allowed to the API!');
});

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, '/frontend/build')));

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/frontend/build/index.html'));
});

app.listen(port, () => {
  console.log('App is listening on port ' + port);
});
