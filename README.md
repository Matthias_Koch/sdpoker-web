# SDPoker-Web

Webservice for the [AMWA-TV SDPoker](https://github.com/AMWA-TV/sdpoker) with a WebGui and an easy HTTP-API.  
It's built with Node.JS and Express for the Backend and with React & React-Bootstrap for the Frontend.  
![alt text](SDPoker-Web_GUI.png "SDPoker-Web GUI")

## Installation

Use the NPM to install the dependencies, build the frontend and then start it.

```bash
cd frontend
npm install
npm run build
cd ..
npm install
npm start
```

## Usage

Access the WebInterface on the Port 5000/tcp or you could change the Port with the Environment-Variable `PORT=5000`.

### API

You also can use the API to check SDPs on the URL `<your-host:port>/api/check?test=checkST2110` with an POST-Request where the SDP is attached to the Body as Plain-Text.
To specify an Test you can use the Query-Parameter `test`. Those Options are availible:
1 `checkRFC4566` runs structural tests, format tests and some field specific tests relevant to SMPTE ST 2110
2 `checkRFC4570` runs source-filter tests relevant to SMPTE ST 2110
3 `checkST2110` (Default) runs through the relevant clauses of the SMPTE 2110-10/20/30 documents, and referenced standards such as AES-67 and SMPTE ST 2022-7, applying appropriate tests

Those tests are performed in an increasing manner, so if `checkST2110` is selcted via `<your-host:port>/api/check?test=checkST2110` all three tests are perormed on the SDP.
The `checkST2110` is the default, so if no `test` Query-Parameter is provided this test will be executed.

There is also the possibility to add the following Flags as additional Query-Parameters to configure the Test. If the Flag is present in the query string it will be enabled.

- `checkEndings`: Check line endings are CRLF, no other CR/LF.
- `whitespace`: Strict check of adherence to whitespace rules.
- `should`: As well as shall, also check all should clauses.
- `noCopy`: Fail obvious copies of the ST 2110-10 SDP example.
- `noMedia`: Fail SDP files which do not include any media descriptions.
- `duplicate`: Expect duplicate streams aka ST 2022-7.
- `videoOnly`: Describes only SMPTE ST 2110-20 streams.
- `audioOnly`: Describes only SMPTE ST 2110-30 streams.
- `channelOrder`: Expect audio with ST2110-30 channel-order.
- `shaping`: Check adherence to traffic shaping specification.
- `useIP4`: All addresses expressed in IP v4 notation.
- `useIP6`: All addresses expressed in IP v6 notation.
- `multicast`: Connection addresses must be multicast.
- `unicast`: Connection addresses must be unicast.

Example Query with some Flags: `<your-host:port>/api/check?test=checkST2110&multicast&duplicate`

You will get back an JSON-Object like the following:

```json
{
  "test": "checkST2110",
  "params": {
    "audioOnly": true
  },
  "sdp": "v=0\no=- 1543226711 1543226711 IN IP4 172.29.80.65\ns=Demo Audio Stream\nt=0 0\nm=audio 46848 RTP/AVP 102\nc=IN IP4 232.130.55.188/32\n\na=ts-refclk:ptp=IEEE1588-2008:EC-46-70-FF-FE-00-CE-DE:0\n\na=mediaclk:direct=0\na=ptime:1\na=maxptime:1",
  "errors": [
    {
      "message": "SDP file includes one or more multicast destinations but does not include any a=source-filter lines as per RFC 4570 Section 3."
    },
    {
      "message": "Line 10: For stream 1, required attribute 'rtpmap' is missing for audio stream.",
      "line": 10
    }
  ]
}
```

## Run as a Container

There is built Version of this Container on [Docker Hub](https://hub.docker.com/r/adihilber/sdpoker-web). It exposes Port 5000/tcp for http, but the Port could also be changed via Env-Vars `-e PORT=5001`.

Example Run Comand:

```bash
docker run -d --name=SDPoker-Web -p8080:5000 -e NODE_ENV=production adihilber/sdpoker-web
```

Or as an Docker-Compse:

```docker-compose
version: "2"
services:
    sdpoker-web:
        image: adihilber/sdpoker-web:latest
        ports:
            - 8080:5000
        environment:
            - NODE_ENV=production
        networks:
            - default
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
