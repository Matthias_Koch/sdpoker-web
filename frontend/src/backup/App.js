import React, { Component } from 'react';
import logo from './logo_team42.png';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Card, Form, Container, Row, Col} from 'react-bootstrap'

import TextAreaWithLineNumber from 'text-area-with-line-number';


class App extends Component {

  state = {
    dataReceived: []
  }
  
  onFormSubmit = e => {
    e.preventDefault()
    const formData = new FormData(e.target),
          formDataObj = Object.fromEntries(formData.entries())
    console.log(formDataObj)

    let url = new URL('/api/check', window.location.origin);
    let search = new URLSearchParams({test: formDataObj.test});
    for(var property in formDataObj) {
      if(property.startsWith('flag')) {
        search.append(formDataObj[property], 'true');
      }
    }
    url.search = search;
    console.log.apply(formDataObj.sdp)
    console.log(url);
    fetch(url, {
      method: 'POST',
      body: formDataObj.sdp,
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data)
 //     console.log(data.errors);
      this.setState({ dataReceived: data.errors });
    });
    }    
  

 

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Container>
            <Card className="mb-3 mt-3 p-3">
              <Row>
                <Col xs={10}>
                  <Card.Text className="title">SDPoker</Card.Text>
                </Col>
                <Col>
                <Card.Img src={logo} alt="Logo Team.42"></Card.Img>
                </Col>
              </Row>
              <Form onSubmit={this.onFormSubmit}>
                <Row className="mt-3">
                  <Col xs={5}>
                  <Form.Label className="label">Tests</Form.Label>
                  </Col>
                  <Col>
                    <Form.Label className="label">SDP Data/URL</Form.Label>
                  </Col>
                </Row>
                <Row>
                  <Col xs={5}>
                  <Form.Group className="mb-3" controlid="RFCRadio">
                      <Form.Check
                        className="tests"
                        type="radio"
                        label="Check RFC4566"
                        name="test"
                        value="checkRFC4566"
                        id="RFC4566Radio"
                      />
                      <Form.Check
                        className="tests"
                        type="radio"
                        label="Check RFC4570"
                        name="test"
                        value="checkRFC4570"
                        id="RFC4570Radio"
                      />
                      <Form.Check
                        className="tests"
                        type="radio"
                        label="Check ST2110"
                        name="test"
                        value="checkST2110"
                        id="ST2110Radio"
                        defaultChecked="true"
                      />    
                    </Form.Group>
                    <Form.Label className="label">Additional Flags</Form.Label>
                  <Form.Group className="mb-3" controllid="Flags">
                      <Form.Check
                        className="flags"
                        label="Check line endings are CRLF, no other CR/LF"
                        name="flag1"
                        value="checkEndings"
                        id="checkEndings"
                      />  
                      <Form.Check
                        className="flags"
                        label="Strict check of adherence to whitespace rules"
                        name="flag2"
                        value="whitespace"
                        id="whitespace"
                      />
                      <Form.Check
                        className="flags"
                        label="As well as shall, also check all should clauses"
                        name="flag3"
                        value="should"
                        id="should"
                      />
                      <Form.Check
                        className="flags"
                        label="Fail obvious copies of the ST 2110-10 SDP example"
                        name="flag4"
                        value="noCopy"
                        id="noCopy"
                      />
                      <Form.Check
                        className="flags"
                        label="Fail SDP files which don't include any media descriptions"
                        name="flag5"
                        value="noMedia"
                        id="noMedia"
                      />       
                      <Form.Check
                        className="flags"
                        label="Expect duplicate streams aka ST 2022-7"
                        name="flag6"
                        value="duplicate"
                        id="duplicate"
                      />
                      <Form.Check
                        className="flags"
                        label="Describes only SMPTE ST 2110-20 streams"
                        name="flag7"
                        value="videoOnly"
                        id="videoOnly"
                      />
                      <Form.Check
                        className="flags"
                        label="Describes only SMPTE ST 2110-30 streams"
                        name="flag8"
                        value="audioOnly"
                        id="audioOnly"
                      />
                      <Form.Check
                        className="flags"
                        label="Expect audio with ST2110-30 channel-order"
                        name="flag9"
                        value="channelOrder"
                        id="channelOrder"
                      />
                      <Form.Check
                        className="flags"
                        label="Check adherence to traffic shaping specification"
                        name="flag10"
                        value="shaping"
                        id="shaping"
                      />
                      <Form.Check
                        className="flags"
                        label="All addresses expressed in IP v4 notation"
                        name="flag11"
                        value="useIP4"
                        id="useIP4"
                      />
                      <Form.Check
                        className="flags"
                        label="All addresses expressed in IP v6 notation"
                        name="flag12"
                        value="useIP6"
                        id="useIP6"
                      />
                      <Form.Check
                        className="flags"
                        label="Connection addresses must be multicast"
                        name="flag13"
                        value="multicast"
                        id="multicast"
                      />
                      <Form.Check
                        className="flags"
                        label="Connection addresses must be unicast"
                        name="flag14"
                        value="unicast"
                        id="unicast"
                      />             
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3" controlid="SDP">
                      <Form.Control
                        className="textarea mr-5"
                        as="textarea"
                        placeholder="{}"
                        name="sdp"
                      />
                    </Form.Group>
                    <Form.Label className="label">Result</Form.Label>
                    <Form.Group className="mb-3" controlid="result">
                      <Form.Control
                        className="resultarea mr-5"
                        as="resultarea"
                        name="result"
                      />
                    </Form.Group>
                  </Col>
                </Row> 
                <Row>
                  <Col xs={5}>
                    &nbsp;
                  </Col>
                  <Col xs={6}>
                    <Button className="custom-btn mb-3" type="submit">Process</Button>  
                  </Col>
                  <Col>
                    <Button className="custom-btn mb-3" type="reset">Reset</Button>  
                  </Col>
                </Row>
              </Form>
                  <ul>
                    {this.state.dataReceived.map((errors) => (
                    <li>{errors.message}</li>
                    ))}
                </ul>
            </Card>
          </Container>
        </header>
      </div>
    );
  }
}

export default App;