import React, { useState } from 'react';
import axios from 'axios';
import logo from './logo_team42.png';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Card, Form, Container, Row, Col } from 'react-bootstrap';

import Editor from 'react-simple-code-editor';

var lines = [];
var sdpErrorMarking = false;

const hightlightWithLineNumbers = (input, showMarking) =>
  input
    .split('\n')
    .map((line, i) => {
      var marked = lines.includes(i + 1) && showMarking ? 'marked' : '';
      return `<span class='editorLineNumber ${marked}'>${
        i + 1
      }</span><span class="content ${marked}">${line}</span>`;
    })
    .join('\n');

const Radio = ({ label, id, handleChange, name, form }) => (
  <>
    <input
      type="radio"
      id={id}
      name={name}
      onChange={handleChange}
      value={id}
      checked={form[name] === id}
    />
    <label htmlFor={id}>&nbsp;{label}</label>
    <br />
  </>
);

const Checkbox = ({ label, id, handleChange, form }) => (
  <>
    <input
      type="checkbox"
      id={id}
      name={id}
      value={id}
      onChange={handleChange}
      checked={form[id]}
    />
    <label htmlFor={id}>&nbsp;{label}</label>
    <br />
  </>
);

function App() {
  const [form, setFormValue] = useState({
    test: 'checkST2110',
    checkEndings: false,
    whitespace: false,
    should: false,
    noCopy: false,
    noMedia: false,
    duplicate: false,
    videoOnly: false,
    audioOnly: false,
    channelOrder: false,
    shaping: false,
    useIP4: false,
    useIP6: false,
    multicast: false,
    unicast: false,
    sdp: '',
  });
  const [sdpErrors, setSDPAnswer] = useState('');

  const [borderNoError, setBorderNoError] = useState(false);

  const [showResponse, setShowResponse] = useState(false);

  function handleChange(e) {
    const { name, value, type, checked } = e.target;

    setFormValue((prevState) => ({
      ...prevState,
      [name]: type === 'checkbox' ? checked : value,
    }));
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    lines = [];

    const urlParams = new URLSearchParams({ test: form.test });
    if (form.checkEndings === true) {
      urlParams.append('checkEndings', 'true');
    }
    if (form.whitespace === true) {
      urlParams.append('whitespace', 'true');
    }
    if (form.should === true) {
      urlParams.append('should', 'true');
    }
    if (form.noCopy === true) {
      urlParams.append('noCopy', 'true');
    }
    if (form.noMedia === true) {
      urlParams.append('noMedia', 'true');
    }
    if (form.duplicate === true) {
      urlParams.append('duplicate', 'true');
    }
    if (form.videoOnly === true) {
      urlParams.append('videoOnly', 'true');
    }
    if (form.audioOnly === true) {
      urlParams.append('audioOnly', 'true');
    }
    if (form.channelOrder === true) {
      urlParams.append('channelOrder', 'true');
    }
    if (form.shaping === true) {
      urlParams.append('shaping', 'true');
    }
    if (form.useIP4 === true) {
      urlParams.append('useIP4', 'true');
    }
    if (form.useIP6 === true) {
      urlParams.append('useIP6', 'true');
    }
    if (form.multicast === true) {
      urlParams.append('multicast', 'true');
    }
    if (form.unicast === true) {
      urlParams.append('unicast', 'true');
    }

    axios({
      method: 'post',
      url: 'api/check',
      params: urlParams,
      data: form.sdp,
    }).then(function (response) {
      console.log(response);
      console.log(response.data.errors);
      console.log(response.statusText);
      let sdpanswer = response.data.errors;
      sdpanswer.forEach((error) => {
        if (error.line) lines.push(error.line);
      });

      const answerString = () => {
        let returnValue = '';
        if (sdpanswer.length) {
          returnValue += sdpanswer.map((errors) => errors.message).join('\n');
        } else {
          returnValue += 'SDP seems to be valid. No Errors!';
          setBorderNoError(true);
        }
        return returnValue;
      };
      sdpErrorMarking = true;
      setSDPAnswer(answerString);
      setShowResponse(true);
    });
  };

  function handleReset() {
    sdpErrorMarking = false;
    setBorderNoError(false);
    setSDPAnswer('');
    setFormValue({
      test: 'checkST2110',
      checkEndings: false,
      whitespace: false,
      should: false,
      noCopy: false,
      noMedia: false,
      duplicate: false,
      videoOnly: false,
      audioOnly: false,
      channelOrder: false,
      shaping: false,
      useIP4: false,
      useIP6: false,
      multicast: false,
      unicast: false,
      sdp: '',
    });
    setShowResponse(false);
  }

  return (
    <div className="App">
      <header className="App-header">
        <Container>
          <Card className="mb-3 mt-3 p-3">
            <Row>
              <Col xs={10}>
                <Card.Text className="title">SDPoker</Card.Text>
              </Col>
              <Col>
                <Card.Img src={logo} alt="Logo Team.42"></Card.Img>
              </Col>
            </Row>
            <Form onSubmit={handleSubmit}>
              <Row>
                <Col xs={5}>
                  <Form.Label className="label">Tests</Form.Label>
                  <Form.Group className="mb-3" controlid="RFCRadio">
                    <Radio
                      form={form}
                      name="test"
                      label="Check RFC4566"
                      id="checkRFC4566"
                      handleChange={handleChange}
                    />
                    <Radio
                      form={form}
                      name="test"
                      label="Check RFC4570"
                      id="checkRFC4570"
                      handleChange={handleChange}
                    />
                    <Radio
                      form={form}
                      name="test"
                      label="Check ST2110"
                      id="checkST2110"
                      handleChange={handleChange}
                    />
                  </Form.Group>
                  <Form.Label className="label">Additional Flags</Form.Label>
                  <Form.Group className="mb-3" controllid="Flags">
                    <Checkbox
                      form={form}
                      label="Check line endings are CRLF, no other CR/LF"
                      id="checkEndings"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Strict check of adherence to whitespace rules"
                      id="whitespace"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="As well as shall, also check all should clauses"
                      id="should"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Fail obvious copies of the ST 2110-10 SDP example"
                      id="noCopy"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Fail SDP files which don't include any media descriptions"
                      id="noMedia"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Expect duplicate streams aka ST 2022-7"
                      id="duplicate"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Describes only SMPTE ST 2110-20 streams"
                      id="videoOnly"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Describes only SMPTE ST 2110-30 streams"
                      id="audioOnly"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Expect audio with ST2110-30 channel-order"
                      id="channelOrder"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Check adherence to traffic shaping specification"
                      id="shaping"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="All addresses expressed in IP v6 notation"
                      id="useIP6"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="All addresses expressed in IP v4 notation"
                      id="useIP4"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Connection addresses must be multicast"
                      id="multicast"
                      handleChange={handleChange}
                    />
                    <Checkbox
                      form={form}
                      label="Connection addresses must be unicast"
                      id="unicast"
                      handleChange={handleChange}
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Label className="label">SDP Data/URL</Form.Label>
                  <Form.Group className="mb-3" controlid="SDP">
                    <Editor
                      value={form.sdp}
                      onValueChange={(code) => {
                        sdpErrorMarking = false;
                        setFormValue((prevState) => ({
                          ...prevState,
                          sdp: code,
                        }));
                      }}
                      highlight={(code) =>
                        hightlightWithLineNumbers(code, sdpErrorMarking)
                      }
                      padding={10}
                      textareaId="codeArea"
                      className="editor"
                      style={{
                        fontFamily: '"Fira code", "Fira Mono", monospace',
                        outline: 0,
                        'min-height': '500px',
                      }}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col className="d-flex justify-content-between">
                  <Button
                    className="custom-btn mb-3"
                    type="reset"
                    onClick={handleReset}
                  >
                    Reset
                  </Button>

                  <Button className="custom-btn mb-3" type="submit">
                    Process
                  </Button>
                </Col>
              </Row>
              <Row className={showResponse ? '' : 'd-none'}>
                <Col>
                  <Form.Label className="label">Errors</Form.Label>
                  <Form.Group className="mb-3" controlid="result">
                    <Editor
                      value={sdpErrors}
                      highlight={(code) =>
                        hightlightWithLineNumbers(code, false)
                      }
                      padding={10}
                      textareaId="ResultArea"
                      className={
                        sdpErrorMarking
                          ? borderNoError
                            ? 'editor noError'
                            : 'editor Error'
                          : 'editor'
                      }
                      style={{
                        fontFamily: '"Fira code", "Fira Mono", monospace',
                        outline: 0,
                      }}
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Form>
          </Card>
        </Container>
      </header>
    </div>
  );
}

export default App;
