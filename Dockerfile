FROM node:14-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY frontend/package*.json ./
RUN npm install --no-optional
RUN npm install react-scripts@4.0.0 -g --silent
COPY frontend/ ./
RUN npm run build


FROM node:14
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
ENV NODE_ENV production
COPY package*.json ./
RUN git config --global url."https://github.com/".insteadOf git@github.com: && git config --global url."https://".insteadOf ssh:// && npm ci --only=production
COPY --from=build /app/build ./frontend/build
COPY . ./

EXPOSE 5000
CMD [ "node", "server.js" ]
